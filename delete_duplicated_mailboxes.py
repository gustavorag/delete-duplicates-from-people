#!/usr/bin/env python
"""
Utility that deletes duplicated records from people table. 
    - Prerequisite - table people_distinct must exist and contain the distincts values from box_fulladdress people's column
        * Run the AP-4524-copy-distincts.sh

Usage: delete_duplicated_mailboxes.py --host=127.0.0.1 --port=3306 --distinct_mailboxes_limit=10 --delete_sql_rows_limit=500 --delay_between_delete_ms=500 --dry-run
"""
import argparse
import os
import time
import logging.config
from contextlib import closing
import MySQLdb
import MySQLdb.cursors
"""
--------------- CONSTANTS AND DEFAULTS ---------------
"""
AKABEANS_DATABASE = "akabeans"

DEFAULT_MYSQL_PORT = 3306
DEFAULT_DISTINCT_MAILBOXES_LIMIT = 10
DEFAULT_DELETE_SQL_ROWS_LIMIT = 500
DEFAULT_DELAY_BETWEEN_DELETE_MS = 500

DB_USER = os.environ['DATABASE_USER']
DB_PASSWORD = os.environ['DATABASE_PASSWORD']

"""
--------------- QUERIES ---------------
"""

CHECK_SQL = "SELECT 1 FROM people_distinct"

SELECT_DISTINCTS = (
  "SELECT box_fulladdress FROM people_distinct LIMIT %s, %s"
)

SELECT_RID_FOR_BOXADDRESS = (
  "SELECT rid, modified_on FROM people WHERE box_fulladdress = %s AND directory_status = 1 LIMIT %s"
)

DELETE_FROM_PEOPLE_BY_RID_NOT_MODIFIED_ON = (
  "DELETE FROM people WHERE rid IN %s AND modified_on < %s" 
)

DROP_DISTINCT_TABLE= (
  "DROP TABLE IF EXISTS people_distinct" 
)


logging.basicConfig(level=logging.INFO)
LOGGER = logging.getLogger(__name__)

def main():
    LOGGER.info("-------------------- Starting delete_duplicated_mailboxes --------------------")
    """
    Process args
    """
    arg_parser = argparse.ArgumentParser(
        description="Utility that deletes orphan records from mailbox table")

    arg_parser.add_argument("--host",
                            dest='host',
                            required=True,
                            type=str,
                            help="Shard host")

    arg_parser.add_argument("--port",
                            dest='port',
                            required=False, default=DEFAULT_MYSQL_PORT,
                            type=int,
                            help="Shard port")

    arg_parser.add_argument("--distinct_mailboxes_limit",
                            dest='distinct_mailboxes_limit',
                            required=False, default=DEFAULT_DISTINCT_MAILBOXES_LIMIT,
                            type=int,
                            help="How many distinct mailboxes will be processed by one iteration")

    arg_parser.add_argument("--delete_sql_rows_limit",
                            dest='delete_sql_rows_limit',
                            required=False, default=DEFAULT_DELETE_SQL_ROWS_LIMIT,
                            type=int,
                            help="How many duplicated records for a distinct mailbox will be deleted by one iteration")

    arg_parser.add_argument("--delay_between_delete_ms",
                            dest='delay_between_delete_ms',
                            required=False, default=DEFAULT_DELAY_BETWEEN_DELETE_MS,
                            type=int,
                            help="Delay between SQL delete for one iteration")

    arg_parser.add_argument("--dry-run",
                            dest="dry_run",
                            required=False,
                            default=False,
                            action="store_true",
                            help="Print destructive SQL statement without running it")

    query_args = arg_parser.parse_args()

    db_credentials = {
        'host': query_args.host,
        'db': AKABEANS_DATABASE,
        'port': query_args.port,
        'user': DB_USER,
        'password': DB_PASSWORD
    }
    db_ok = check_connection(db_credentials)
    if not db_ok:
        LOGGER.info("people_distinct table not found or database not available")
        return

    _delete_duplicated_mailboxes(db_credentials, query_args)

def _delete_duplicated_mailboxes(db_credentials, query_args):

    dry_run = query_args.dry_run

    """
    For each iteration, a limited list of distinct mailboxes are selected from the people_distinct table
    For this selection, an OFFSET and LIMIT is used for the query, incrementing it by the list size limit
    For example: 
        - iteration 1, OFFSET=0, LIMIT=10
        - iteration 2, OFFSET=10, LIMIT=10
        - iteration 3, OFFSET=20, LIMIT=10
        - and so one
    """
    offset = 0

    iteration = 1
    while True:
        LOGGER.info("-------------------- Iteration %s --------------------", iteration)
        total_processed = _process_distinct_mailboxes_by_limit(db_credentials, offset, query_args, dry_run)
        if total_processed is None or total_processed < 1:
            break
        iteration = iteration + 1
        
        # Set the offset for next iteration
        offset = offset+query_args.distinct_mailboxes_limit

    with closing(get_mysql_connect(db_credentials, db_cursor=MySQLdb.cursors.DictCursor)) as config_connect, \
        closing(config_connect.cursor()) as db_cursor:
        LOGGER.info("-------------------- DROPPING people_distinct table --------------------")
        db_cursor.execute(DROP_DISTINCT_TABLE)


def _process_distinct_mailboxes_by_limit(db_credentials, offset, query_args, dry_run):
    
    total_processed = 0
    mailbox_limit = query_args.distinct_mailboxes_limit
    delete_limit = query_args.delete_sql_rows_limit

    with closing(get_mysql_connect(db_credentials, db_cursor=MySQLdb.cursors.DictCursor)) as config_connect, \
            closing(config_connect.cursor()) as db_cursor:
        
        _run_sql(db_cursor, SELECT_DISTINCTS, [offset, mailbox_limit], dry_run)
        results = db_cursor.fetchall()

        if results is None or len(results) < 1:
            return total_processed

        for mailbox in results:

            start_time = time.time()
            total_processed = total_processed +1
            box_fulladdress = mailbox['box_fulladdress']
            total_deletes = 0
            
            while True:
                
                # Selecting the batch of rid of duplicated box_fulladdress to be deleted
                _run_sql(db_cursor, SELECT_RID_FOR_BOXADDRESS, [box_fulladdress, delete_limit], dry_run)
                duplicates = db_cursor.fetchall()

                if duplicates is None or len(duplicates) < 2: #if has only one record, it means there is nothing to delete
                    break
                #End if

                mostRecent = None

                ridToDelete = list()
                for d in duplicates:
                    if mostRecent is None or d['modified_on'] > mostRecent:
                        mostRecent = d['modified_on']
                    ridToDelete.append(int(d['rid']))
                
                if len(ridToDelete) > 1: #If there is only one, no need to delete
                    total_deletes = total_deletes + len(ridToDelete)
                    _run_sql(db_cursor, DELETE_FROM_PEOPLE_BY_RID_NOT_MODIFIED_ON, [tuple(ridToDelete), mostRecent], dry_run)

                config_connect.commit()

                LOGGER.info("Sleep %s milliseconds between delete", query_args.delay_between_delete_ms)
                time.sleep(query_args.delay_between_delete_ms / 1000)
                
            #End while
            if total_deletes > 0:
                elapsed_time = time.time() - start_time
                LOGGER.info("Elapsed time: %.3f sec", elapsed_time)
                LOGGER.info("Deleted %s rows for `box_fulladdress`: %s", total_deletes, box_fulladdress)
        #End for

    #End with
        
    return total_processed


def _run_sql(db_cursor, sql, arguments, dry_run):
    tuple_arg = tuple(arguments)
    if dry_run:
        # print SQL code
        LOGGER.info("SQL: %s", sql % tuple_arg)

    # execute all statement if not(dry_run) or only if select statements for dry_run mode
    if not dry_run or sql.lower().startswith('select'):
        result = db_cursor.execute(sql, tuple_arg)
        return result

def get_mysql_connect(db_credentials, db_cursor=MySQLdb.cursors.Cursor):
    """
    Get MySQL connect
    """
    if db_credentials['db'] is None:
        db_credentials['db'] = ""

    db_connect = MySQLdb.connect(
        host=db_credentials['host'],
        port=db_credentials['port'],
        db=db_credentials['db'],
        user=db_credentials['user'],
        passwd=db_credentials['password'],
        cursorclass=db_cursor
    )

    return db_connect

def check_connection(db_credentials):
    """
    Check whether we have a connection to the required database or not.
    """
    with closing(get_mysql_connect(db_credentials)) as db_connect, \
        closing(db_connect.cursor()) as db_cursor:
        try:
            db_cursor.execute(CHECK_SQL)
            return True
        except Exception as e:
            LOGGER.error("Error: %s --------------------", e)
            return False

if __name__ == '__main__':
    main()