#/bin/bash.
set -eu -o pipefail
    
HOST=${1:-}
    
if [[ -z $HOST ]]; then
    echo "Usage: AP-4524-copy-distincts.sh <host>"
    exit 1
fi
if [[ -z "${DATABASE_USER:-}" ]]; then
    echo "Please set environment variable 'DATABASE_USER'"
    exit 1
fi
if [[ -z "${DATABASE_PASSWORD:-}" ]]; then
    echo "Please set environment variable 'DATABASE_PASSWORD'"
    exit 1
fi
    
mysql -h $HOST -u ${DATABASE_USER} -p${DATABASE_PASSWORD} --database=akabeans -e "CREATE TABLE people_distinct (distinct_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 SELECT distinct(box_fulladdress) FROM people WHERE TRIM(box_fulladdress) <> '';"
echo "Table people_distinct created"
